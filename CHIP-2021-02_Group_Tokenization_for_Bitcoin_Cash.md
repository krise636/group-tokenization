# CHIP-2021-02 Group Tokenization for Bitcoin Cash

        Title: Group Tokenization for Bitcoin Cash
        First Submission Date: 2021-02-23
        Owners: Anonymous Contributor A60AB5450353F40E
        Type: Technical
        Layer: Consensus, Network
        Status: DRAFT
        Last Edit Date: 2021-04-26

## Summary

This proposal describes an upgrade to the Bitcoin Cash peer-to-peer electronic cash system that would enable native support for alternative currencies, without impeding on utility of the one native currency (BCH).
It does so by introducing dual-currency outputs (BCH & token), by having a set of BCH outputs grouped by the token.
Throughout the proposal we refer to these user created alternative currencies as "group tokens" or simply "tokens", while the proposal is more formally called Group Tokenization.

With Group Tokenization each transaction output that contains a token also contains some amount of BCH.
At a minimum such outputs will always be weighted down by the BCH dust limit.
Consequentially, every token output marginally increases demand for BCH, and the incentive for users to consolidate the UTXO set is not affected.

Since every token output is also a BCH output, these outputs will function the same as native currency, thus remaining faithful to design precepts from the Bitcoin whitepaper.

## Motivation

The first real use of Ethereum smart contracts was to implement tokens.
From there the market has seen a token cambrian explosion, which ironicaly made smart contracts useful.
The demand for tokens is seen in growth of [tokens market cap](https://coinmarketcap.com/tokens/views/all/) and market appreciation of native currencies that power them.
Not so long ago, Binance launched a [tokenized stock solution](https://www.binance.com/en/support/articles/2c64611658c645a59e05ef12f02c22ab) that made big news and market cap of the native BNB token made a new ATH.

Users don't use a technology, they use a product built with a technology.
We believe that this proposal would give product builders the building blocks to create amazing products, and attract both enterpreneurs and users to Bitcoin Cash ecosystem.

Most tokens are used to represent some form of cash or asset.
With the simplest token system we could express fiat currency, stocks, bonds, tickets, real estate deeds, and much more; which are mostly cash-like uses.
This proposal could enable products of profound value to people who can't trust their governments to track ownership.

Many of those people are already using Bitcoin Cash because they can't trust their government's money, either, so Bitcoin Cash already has a foothold there.
This proposal would put more power into their hands by enabling them to independently build products to solve many more of their problems by using the tools provided by the Bitcoin Cash blockchain and ecosystem.
By solving their problems they would also expand utility and increase the value of Bitcoin Cash: The Peer-to-Peer Electronic Cash System for Planet Earth.
With this, Bitcoin Cash could gain a first mover advantage in areas beyond reach of the current cryptocurrency craze, an investment into people which could grow as those areas develop.

For others who can depend more on legacy systems, this proposal would mean that Bitcoin Cash could become more closely integrated with existing and future financial systems and tap into value currently unavailable.

This also means that Bitcoin Cash could connect the two worlds, to allow people from underdeveloped world inclusion into financial systems of the developed world.
It could enable unregulated stock markets in underdeveloped countries, tradable on exchanges in developed countries, or on a native decentralized exchanges accessible by everyone.
It could enable startups from underdeveloped areas to raise funds for development and issue shares in their compaines.
With this, Bitcoin Cash community could be the one providing them with a ladder, and we feel that helping others grow will help Bitcoin Cash grow.

If a simple token system could unlock so much utility by broadening the "cash" scope of our peer-to-peer electronic cash system, then we believe it would be reasonable to have that system be as efficient and easy to use as possible.
We cannot be expected to build complete solutions for everyone, that would be falling into the central planner trap.
We should instead provide everyone with the tools to easily build soultions they need by themselves.
The way to achieve that would be to enforce token logic by the same "contract" which enfoces BCH logic -- the native consensus code (C++) contract.

This way, tokens could enjoy the same security guarantees, simplicity and ease of use as the native currency while benefitting from all the features available to the native currency, such as SPV, SIGHASH, and Script.
Combined with those existing features tokens could enjoy functionality such as token atomic swaps, decentralized exchange, and token CoinJoin/CashFusion.

When compared with Ethereum, group tokens are conceptually closer to [ERC-1155](https://blog.enjincoin.io/erc-1155-the-final-token-standard-on-ethereum-a83fce9f5714) than ERC-20 in that Group Tokenization is also just one global contract, where users will interact with it by spending and crafting transaction outputs.

## Technical Description

This section intends to help the reader build a mental model of group tokenization.
It is intended for illustrative purposes and the careful, authoritative specification is located in the [Specification](#specification) section.
We introduce the scope starting from the most minimal token system, and then build it up to the full scope of this proposal.

For those readers familiar with the proposal, note that through interaction with various stakeholders of the Bitcoin Cash ecosystem it has evolved since being first introduced and now significantly differs from Andrew Stone's [original proposal](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs).
The base token concept is the same, but the "authority" system has been constrained to just one UTXO for each distinct token, and is now called the token baton.

### Token Transfer System

Bitcoin Cash transactions are designed to move money, always in the form of its native token labeled BCH and using the atomic unit "satoshi".
This proposal intends to enable movement of other tokens using the same means as BCH.

We introduce a dual-currency output system (BCH & token), having every token amount always be grouped with an amount of BCH.
Token amounts state is tracked using two new and optional fields attached to BCH outputs, a "Token ID" to distiguish tokens, and a "Token quantity" to record their amounts.
Previous transaction outputs are used as inputs to a new transaction where they will be unlocked and consumed, and their BCH and token balance moved to newly created unspent dual-token transaction outputs.

<p align="center"><img src="./CHIP-2021-02_fig1.png"></img></p>

These new types of outputs can be freely mixed and combined with any number of dual (token & BCH) and pure (BCH) inputs and outputs in the same transaction.
The main rule is that the amounts in, summed for each disctinct token, have to be equal to the amounts sent out.
This is almost the same rule that's already enforced for BCH balances.

Balancing of BCH inside token outputs is enforced independently with existing BCH validation rules, which are left unchanged.
As a consequence, BCH can freely flow between pure and dual outputs but dual outputs cannot exist without at least a dust limit amount of BCH.
Any excess BCH on the input side will be claimed by miners as fee, and token amounts have to exactly balance.

Token outputs can be permissionlessly spent to a 0-satoshi OP_RETURN token output, and the token amount will then become unspendable while the BCH dust it contained will be reclaimed to another output or used for miner fee payment.

To avoid introducing a new transaction format for token data and enable token outputs to freely mingle with pure BCH outputs, we propose to insert the required fields inside the TX output and as a prefix to the output locking script.
The full grouped output serialization is presented below:

*<satoshiValue\><scriptLength\>* `[PFX_GROUP <tokenID> <qty>]` *<lockingScript\>*

where:  
`PFX_GROUP == 0xEE` which matches a currently unused and disabled Script opcode  
`<tokenID>` 32-byte token identifier, universally unique at token creation  
`<qty>` [VarInt](https://reference.cash/protocol/formats/variable-length-integer) token quantity

We will refer to the full prefix with its arguments as "group annotation".

This way unupgraded wallets would treat such annotated outputs as unspendable due to disabled pubkey script.
The annotation is used only by the Group Tokenization consensus sub-system and, when activating the Script VM parser, the annotation will be sliced off and only the `<lockingScript>` handed over to it.

With the above rules we have created a token TRANSFER system, but we don't have a way for tokens to enter that system to begin with i.e. first token units need to be minted somehow.

### Token Genesis

The GENESIS TX creates the first output with a new `tokenID`, called the genesis output.
The genesis output must only satisfy the rule:

`PFX_GROUP <tokenID = H(genData)> <qty>`

Where  
`H` is the SHA256 hash function, and  
`genData` is byte concatenation of:

- First prevout TXID
- First prevout vout index
- The entire genesis output being generated, with the tokenID left out
- The vout index of the genesis output being generated

A new token is created simply by spending any output, and creating new token outputs which satisfy the GENESIS criteria and will therefore each have a new universally unique tokenID "issued" pseudo-randomly.

With this, token genesis is self-evidently valid or invalid and not depending on other token inputs or non-local data. It is possible to create any number of distinct tokenID genesis outputs in a single TX while making it computationally impossible to collide with an existing tokenID to create a duplicate token genesis. With 256 bits of entropy Wagner's birthday attack is impossible for any tokenID, against which it will have 128 bits of security.

Note that the risk of a birthday attack is not the same as with Bitcoin Cash addresses, where collisions are possible with the 160-byte address format.
A colliding address pair will have the same owner and is generally not a problem.
A colliding token ID could be used to post another GENESIS TX after it's already been created for that token ID, and destroy confidence in the whole token system.

With this we have created a token system that allows us to: create a token supply and then transfer it the same way as BCH.

Even this minimal token system can, through the use of existing SIGHASH feature, perform multi-party multi-token atomic swaps between tokens and BCH, and so enable token & BCH DEX, and token CoinJoin/CashFusion.
This is later demonstrated in the [examples](#example-uses) section.

With this system, however, the issuer would be very limited in managing the supply as it can be created only once at genesis.
With such a limit, it would be very awkward to implement something like a stablecoin where token supply has to be changed to match the underlying asset.  To provide such functionality a Token Baton is provided.

### Token Baton

For creating new outputs post-genesis we must first introduce the concept of the token baton.
It is a special type of token output which, when spent in a transaction, allows that transaction to mint or destroy token amounts.
It may help to think of the baton as a "magical" token amount, which can be fanned out to any number of token amounts while ignoring the token balancing rule.
At the same time, it must carry exactly one instance of itself to the output side as "baton change", so there will only exist one baton UTXO for each distinct tokenID until given up by being spent to an OP_RETURN script.

The baton behaves the same as any other BCH or token output so can use all the interfaces available to BCH and token outputs, and change ownership the same way. With the introduction of the baton here, we will restrict the GENESIS rule to only allow the baton to be created as the genesis output.

Validation rules will need to differentiate token amounts and the token baton, and so we update the group annotation format to allow us to carry a bitfield and minting capacity:

`PFX_GROUP <tokenID><qty>` - ordinary token amount  
`PFX_GROUP <tokenID><0x00><batonFlags><leftToMint>` - token baton

Where:
`batonFlags` is a 1-byte bitfield, which should be deserialized as a VarInt for implementation consistency, and  
`leftToMint` is a VarInt of any supported size (1, 2, 4, 8 uint when deserialized)

Token 0-amounts are therefore disabled, and instead encode the baton.

The flags can only be set at ganesis.
Once unset, they can not be set back.

We now introduce only one baton flag:  
MINT ==  0x01, to allow minting or rescinding tokens back into the minting pool.  

#### MINT Baton Flag

Presence of a MINT flag allows minting token amounts from the `leftToMint` minting pool.

Token amounts created in a transaction must balance with `leftToMint` amounts on both sides of the transaction.

The mint baton allows token amounts to be either destroyed or rescinded  put back into the minting pool.
This allows either reducing the maximum supply or taking tokens out of circulation without removing them from the maximum supply.
With a MINT baton, the balancing rule becomes:

`in_tokenSum + in_leftToMint >= out_tokenSum + out_leftToMint`

So the supply cap will always be the one set at genesis, and the total minted amount can be calculated simply by subtracting the baton UTXO from the genesis TXO.

Unsetting the MINT flag locks the ability to use whatever remaining minting pool, and the baton will then only allow token amounts destruction, where the balancing rule becomes:

`in_tokenSum >= out_tokenSum`

#### Metadata Baton Extension

It is a common problem for token systems to keep track of token metadata.
Here we introduce a way to ensure that the state of metadata is correctly tracked, and enable users to lock parts of metadata from modification while allowing the metadata field defitinions to be customized by downstream application protocols.

We extend the baton flags with metadata locks, and the group annotation with metadata data fields.
The current metadata should always be contained in the baton UTXO:

`PFX_GROUP <tokenID><0x00><batonFlags><leftToMint> <<metaLen1>[metaData1]... <metaLen5>[metaData5]>`

When a locks bit is toggled it means **unlocked**, and the added flags are:  
LOCK_1 == 0x02, permits editing the 1st metadata field.  
LOCK_2 == 0x04, permits editing the 2nd metadata field.  
...  
LOCK_5 == 0x20, permits editing the 5th metadata field.  

The metadata flags and data fields have no impact on token balancing rules.

The 5 `LOCK_X` flags refer to metadata fields in order, and ALL 5 fields MUST be included in every baton output.
All locked fields must be the same on both sides of a transaction.

A lock can be locked only if the field it applies to hasn't been changed in the same transaction.
This is to prevent accidential change & lock in the same transaction, which would make it impossible for the user to correct an errorneus update which also locked the field.

NULL fields are allowed and specified as `<0x00>` without the `[metaDataX]`.

There are only 5 hardcoded fields for storing arbitrary data. By default, in the absense of another application specific specification, wallet clients and explorers should use the following interpretation and encoding:

metaData1: `decimals`; 1 byte, in range 0x00-0x09 (same as the slp specification)
metaData2: `ticker`; any length, UTF-8
metaData3: `name`; any length, UTF-8
metaData4: `document url`; any length, UTF-8
metaData5: `document sha256`; 32-bytes (same as the slp specification)

Downstream application protocols and wallets are free to change the meaning of these arbitrary data fields as they wish.  Those protocols can clearly signal to override the defaults above using some magic bytes within the `metaData1` field outside of the default range 0x00-0x09.

The `isStandard` network rule could restrict the sizes for each field, allowing for a possibility of adjusting them if there will be user need.

To make the `isStandard` check simpler, the length of the whole metadata extension could be fixed, and users could 0-pad one of the fields to match the length.

Changing the number of lockable fields and locks would require a hard fork upgrade.

### Additional Genesis Constraints

It is possible to encode some minimal information into the tokenID by re-rolling the genesis output's `tokenID = H(genData)` until a few bytes match the desired value, similarly to how Bitcoin Cash vanity addresses are generated.

We will make use of this system to encode some useful information for the recipient -- from which he can determine immutable token features without having to inspect the genesis output or current token state.

#### Future Upgradeability

For future upgradeability of the token sub-system itself, we require the 1st LSB byte of the tokenID indicate the currently proposed set of consensus rules and be:

ONE_TOKEN_STANDARD == 0x01.

With this, a path remains open to cleanly upgrade the Group Tokenization ruleset in a backwards-compatible and non-breaking way.

Any upgrade that would break current assumptions about tokens could be introduced alongside it, where adhering to the new rules would be opt-in and available only to newly created tokens.

#### Token Types

Because under the "one token standard" there can only be one baton for each tokenID, immutable features of such tokens are entirely deducible from inspecting the genesis baton output.
For simplicity, we will allow only a specific set of genesis baton setups and assign them with a token type.
The token type will then be encoded "inside" the tokenID hash.

These rules apply only to the genesis baton:

- If MINT is set, then leftToMint MUST be > 0.  
- If MINT is unset, then leftToMint MUST be == 0.  
- Metadata CAN be set and locked at the same time.

We require the 2nd LSB byte to encode the genesis setup:

`TOKEN_TYPE == batonFlags & mintOne`

Where:  
`mintOne == 0x80` if leftToMint == 1,  
`mintOne == 0x00` if leftToMint != 1.

The baton flag 0x80 must be DISABLED not to conflict with this scheme.

This makes token types easily identifiable by their `tokenID`:

0x0101... - standard token,  
0x013F... - standard token with entire metadata locked at genesis,  
0x0100... - baton-type NFT,  
0x013E... - baton-type NFT with metadata locked at genesis,  
0x0181... - token-type NFT,  
0x01BF... - token-type NFT with metadata locked at genesis.

and also doubles as a PoW puzzle for token genesis.

## Specification {WIP}

This section describes the blockchain and consensus changes required to implement Group Tokenization on Bitcoin Cash.
It is intended for implementers, and generally does not explain why certain decisions were made.
It assumes that you understand the general architecture and purpose of Group Tokenization, which was described in the sections above.

**Notes on reading this section**:

 * Industry-standard conventions with respect authoritative and normative statements are followed; that is, use of capital MUST, MUST NOT, et. al.
 * Testable requirements are specified with an identifier (REQX.Y.Z) for use when building unit tests.

### 1 Blockchain Changes

Group Tokenization fits its changes within the existing transaction format by inserting the group annotation inside the output by prefixing the output script.
This makes Group Tokenization backwards-compatible with non-upgraded wallets which will ignore such outputs.

#### 1.1 Transaction Changes

Within a transaction, grouped outputs follow the existing output serialization format, with the group annotation inserted as follows:

Ordinary grouped output:  
*<satoshiAmount\><scriptLength\>* `[PFX_GROUP <tokenID><qty>]` *<pubKeyScript\>*

Baton grouped output:  
*<satoshiAmount\><scriptLength\>* `[PFX_GROUP <tokenID><0x00><batonFlags><leftToMint> <<metaLen1>[metaData1]... <metaLen5>[metaData5]>]` *<pubKeyScript\>*

Where `qty`, `batonFlags`, `leftToMint`, and `metaLenX` are encoded as a [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer),  
and PFX_GROUP = 0xEE.

### 2 Script Machine Changes

The byte PFX_GROUP MUST be disabled.

### 3 Transaction Validation Changes

All existing transaction validation rules are unchanged.
However, the PFX_GROUP instruction and its arguments MUST NOT be passed to the Script VM.

An UTXO is defined as "grouped" if it contains a serialized script following the format specified in section 1.1.
The PFX_GROUP with all the arguments is referred to as "group annotation".

In other words, for Group semantics to apply to an UTXO, the real script must be **prefixed** by the group annotation.
PFX_GROUP is therefore semantically an attribute of the UTXO that is actually used *outside* of the script interpreter.
It is structured in this manner to minimize changes to the transaction format, and therefore changes required by the BCH ecosystem.
A non-tokenized, un-upgraded light wallet will understand the transaction and will even be able to spend non-grouped UTXOs from a transaction that has grouped UTXOs.
But it will not understand the grouped UTXOs.
This is a feature that prevents unupgraded light wallets from accidentally spending potentially valuable tokens for their dust BCH.

#### Baton Flags

Baton flags encode possible operations with the baton:

DISABLED_1 = 1ULL << 7, *Disabled, must be zero*  
LOCK_5 = 1ULL << 6, *Can change the 5th metadata field*  
LOCK_4 = 1ULL << 5, *Can change the 4th metadata field*  
LOCK_3 = 1ULL << 4, *Can change the 3rd metadata field*  
LOCK_2 = 1ULL << 3, *Can change the 2nd metadata field*  
LOCK_1 = 1ULL << 2, *Can change the 1st metadata field*  
MINT = 1ULL << 0, *Can mint or rescind tokens*

METADATA_FLAG_BITS = LOCK_1 | LOCK_2 | LOCK_3 | LOCK_4 | LOCK_5  
ACTIVE_FLAG_BITS = MINT | METADATA_FLAG_BITS | DISABLED_1  
ALL_FLAG_BITS = ~0ULL  
RESERVED_FLAG_BITS = ACTIVE_FLAG_BITS & ~ALL_FLAG_BITS

Reserved flag bits 8 through 63 are reserved for future hard fork group features and must be zero.

Note: bare presence of a baton output allows token burning, regardless of the combination of flags.

#### Baton Minting Pool

The `leftToMint` encodes the minting pool of the baton, and is an 8-byte uint serialized as VarInt, and the full uint range of values is allowed.

#### Baton Metadata

The metadata is structured as five variable-length arbitrary data fields of the (len, data) format.
The length CAN be 0 and indicates a NULL field for which the data part SHALL NOT be present.

metaLen1, metaLen2, metaLen3, metaLen4 and metaLen5 are 8-byte unsigned integers, serialized as VarInt.

{TBC: consensus size limits, and isStandard limits}

#### 3.1 Transaction Group Enforcement

Certain properties are enforced across the inputs and outputs of a transaction.
These properties only apply to "grouped" inputs and outputs.
If a transaction has no grouped inputs or outputs an optimized group tokenization validator may return VALID without further analysis.

This section enforces the following general rules:

 * for the same group, input quantities must match output quantities (unless overridden by the baton),
 * baton attributes are properly passed from input to output batons, and
 * newly created groups are created correctly.

This is not a lot of rules and the resulting algorithms are not complex.
However, the following sections specify the exact rules in great detail, covering every tiny edge condition since any differences might cause a consensus failure.
So upon the first read, these sections may make the rules seem daunting.
But actually, this document is much larger than a quality implementation which can fit into a single function and a few helpers.
Just keep your mind on the overall picture stated in the previous paragraph and work through the rules slowly.
A good approach is to loop through outputs then inputs collecting data on each group.
Then loop through the collected groups, verifying the rules based on data collected.
If you come upon an edge condition that seems unspecified make an in-code note that will result in a compilation error.
DO NOT GUESS OR SKIP!
Contact an owner of this CHIP and we will clarify the requirement.

**Notes on reading these rules**:

* All operations proceed on a per token group basis and so "per tokenID" is implied in every statement.
* References to "group inputs" and "outputs" never include the baton -- the baton is special and always specifically referenced.
* Grouped UTXOs have 2 fields: a "tokenID" and "qty", and the grouped baton UTXO has 8 fields: "tokenID", "batonFlags", "leftToMint", and the 5 metadata fields, as described in section 3.1.
* Non-compliance with any of these rules results in an immediate return of INVALID.

For example, the phrase "input xxx baton" (e.g "input mint baton") means "an input group baton with the xxx flag bit set", or to be extremely pedantic, "a transaction input whose prevout UTXO script TX field begins with `[PFX_GROUP <tokenID><0x00><batonFlags><leftToMint> <<metaLen1>[metaData1]... <metaLen5>[metaData5]>]`, and whose tokenID field matches the one we are currently working on and whose `batonFlags` field has the MINT (0) bit set".

Likewise, the phrase "sum of the output Quantities" should be pedantically understood as "the sum of the Quantity fields in non-baton outputs whose tokenID field matches the one we are currently working on".

##### Properly Constructed Group Annotations

* For every output where script TX field begins with the PFX_GROUP byte:
	* The next 32 bytes are read as `<tokenID>`
	* Next is a VarInt, encoding an 8-byte unsigned integer and read as `<qty>`
	* If the `<qty>` is > 0, it is a group token amount, and next after is the **pubkeyScript**
* For every output where the `<qty>` is == 0, continue reading the output as a baton.
	* Next after the 0-qty is a VarInt, encoding an 8-byte unsigned integer and read as `<batonFlags>`
	* Next after the `<batonFlags>` is a VarInt, encoding an 8-byte unsigned integer and read as `<leftToMint>`
    * Next after the `<leftToMint>` is a VarInt, encoding an 8-byte unsigned integer and read as `<metaLen1>`
        * If the `metaLen1` is == 0, then next after is `metaLen2`
        * If the `metaLen1` is > 0, then next after is `metaData1` read as metaLen1 bytes blob, and the next after is `metaLen2`.
        * This is repeated until the `metaLen5` is reached
            * If it is == 0, then next after is the **pubkeyScript**
            * If it is > 0, then next after is `metaData5` read as metaLen5 bytes blob, and the next after is the **pubkeyScript**.

##### Setup

{TODO}

##### Baton Flags Accumulation

{TODO}

##### Baton Metadata Accumulation

{TODO}

##### 3.1.2.1 Group Genesis Rule

{TODO}

##### 3.1.2.2 Baton Rules

{TODO}

##### 3.1.2.3 Token Quantity Rules

{TODO}

##### 3.1.2.5 Grouped P2SH

{TODO}

### 4 IsStandard Changes

{TODO}

### Footnotes

{TODO}

## Appendix - Consensus Rules Flowchart

{TODO}

## Appendix - Example Uses

{TO BE UPDATED}

[Stablecoin Example](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#stablecoin-example)  
[Atomic Swaps](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#atomic-swaps)  
[CoinJoin](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#coinjoin)  
[NFTs](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#nfts)

## Activation Costs and Risks

{note: section is incomplete, still a draft}

The solution is conceptually simple, which means that anyone new to the solution should not have much trouble understanding it, and then following the steps to implement it.
The entire implementation showcasing it can fit into a single `.cpp` file consisting of a single function using simple language structures and which can fit well under ~400 lines of C++.
This number comes from an actual implementation that was focused on understandability, not succinctness.
By reusing existing loops and other techniques, it can be implemented in even fewer lines.
This kind of solution is easy to implement and reason about.
We cannot invent problems with this approach just to satisfy the reader looking for problems.
If we have missed something in our reasoning, we will be happy to hear from you.

### Costs

The cost is one-off workload increase for node developers, after which the tokens will continue to provide utility even if their consensus layer would be frozen.
Even as such, tokens would automatically benefit from other upgrades such as new Script opcodes, so the benefits could increase in the future even with group tokenization itself remaining unchanged.

Downstream, middleware developers can rely on the same design patterns as Bitcoin uses which have been successfully tested since 2009.

{add a note re. possible output sizes}

### Risks

If we define risk as [probability times severity](https://en.wikipedia.org/wiki/Risk#Expected_values) then we could make an estimate using the [risk matrix](https://en.wikipedia.org/wiki/Risk_matrix).

Given the layer on which this is to be implemented, the probability of bugs and network splits is entirely controlled by node developers and quality of their work.
We don't recall a case where different node implemetations disagreed about the way to balance the BCH amount of a transaction and we think it's reasonable to expect that it shouldn't happen with group amounts, either.
After the node software QC process is completed, the probability of a bug should be very low.
If deemed necessarry, formal verification should be possible.

In case of a problem, the severity would depend on which node implementation forked off i.e. their hashpower proportion.
The consequence should be temporary unavailability of the network to everyone relying on that node implementation, and it is in the nature of such events that they can be detected and rectified quickly.

## Ongoing Costs and Risks

{note: section is incomplete, still a draft}

We don't see a significant increase in ongoing costs and risks.
Node implementation should have been verified watertight before activation, and ongoing costs are then moved to middleware which will start to use the building blocks provided.
The risk of wrongful middleware implementation would mostly have the consequence of not seeing the funds, and it would be really hard to actually lose the funds.

On consensus layer, some consideration will have to be given to existence of group code.

Listed below are some changes that would not be significantly affected by having group tokens:

- Network rules, unless we choose to give special treatment to group UTXOs with regards to fee or dust limit requirements
- Blocksize changes, unless we choose to have a separate cap for token TXes
- Pruning
- New opcodes
- New SIGHASH
- New signature types
- UTXO commitments

and some which might be:

- Changing numerical precision for BCH & tokens, and it should be possible to change BCH precision independently of tokens
- Totally new state scheme e.g. [utreexo](https://dci.mit.edu/utreexo)
- ?

## Activation Benefits

{note: section is incomplete, still a draft}

## Ongoing Benefits

{note: section is incomplete, still a draft}

## Implementations

[Showcase implementation](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/src/consensus/grouptokens.cpp)

[Working demo](https://www.nextchain.cash/groupTokensCliExample)

Note that both of the above have been developed to support a broader set of features.

## Test Cases

{note: section is incomplete, still a draft}

[grouptoken_tests.cpp](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/src/test/grouptoken_tests.cpp)
[grouptokens.py](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/qa/rpc-tests/grouptokens.py)

(test vectors) TBD

## Security Considerations

{note: section is incomplete, still a draft}

Not even a single line of existing consensus code needs to be changed to implement Group Tokenization.
So implementations should be able to provably show that the addition of Group Tokenization cannot affect the security of the BCH token.

Group Tokenization changes do not add any additional network messages, so do not increase the network attack surface.

Improper implementations of Group Tokenization could cause a blockchain hard fork.
This is true of any miner-validated tokenization system, or of any new feature requiring a hard fork.

## Evaluation of Alternatives

{note: section is incomplete, still a draft}

We see not having the feature as the only alternative.
Other token proposals are implemented with smart contracts therefore not on the same utility level as native currency and are conceptually different from how the native currency operates.
Our proposal conceptually operates the same way as native currency so downstream developers should not have too much trouble implementing it if they're already familiar with how Bitcoin works.

Other smart contract proposals could operate on grouped tokens, and therefore benefit from grouped tokens, because group tokens provide built-in nouns and a select set of low level verbs.

More generally, we see alternatives in only the rollout approach:

1. We enable native tokens through this proposal,
2. Smart contract solutions either roll their own tokens or use native tokens;

or a simultaneous rollout, or:

1. Roll out a smart contract platform that can roll their own tokens,
2. Wait for the market to demand something more efficient, and then enable native tokens.

The latter approach has the biggest cost in:

- Missed opportunity in the time it takes to build smart contract token systems and evaluate the market response.
- Difficulty in consistently identifying and displaying different smart-contract based tokens in user wallets.
- Technical debt of downstream users having to adapt to new design patterns introduced by the smart contract platform.

We can wait for the market, but the market won't wait for us.
We believe it's reasonable to anticipate that demand if Bitcoin Cash is to get ahead.

We see simultaneous rollout as the preferred option because then synergetic benefit could be achieved right from the start if new smart contracts features would be designed in such way that they can fully use group tokens.

For this to happen, we invite other teams to consider that approach at this early stage in the upgrade cycle, and then a set of features more powerful than any single team could deliver on their own could become available on Bitcoin Cash.

### Industry Acceptance

#### IOTA

The proposed [IOTA tokenization](https://blog.iota.org/tokenization-on-the-tangle-iota-digital-assets-framework/) is a functional subset of Group Tokenization, but notably uses the exact same technique of annotating UTXOs with token identifiers.

#### ION

[ION tokens](https://ionomy.com/) are ported directly from Andrew Stone's work.

#### Blockstream Liquid

["Confidental Assets"](https://blockstream.com/bitcoin17-final41.pdf) uses the same general approach of "...attaching to each output an asset tag identifying the type of that asset, and having verifiers check that the verification equation holds for subsets of the transaction which have only a single asset type."

## Stakeholder Cost and Benefits Analysis

We present a stakeholder taxonomy.
Everyone should be able to find themselves within at least one group.
We attempt to evaluate the impact of this development on all stakeholders and we invite you to join us, add your names to this list, and comment whether you agree with our assesment of the impact.
We're happy to address any concerns or questions you may have.

General costs and benefits:

- (+) Competitive advantage in comparison to other blockchains/ecosystems.
- (+) This functionality will enable competition from within our ecosystem.
Some may feel threatened by this, but what is the alternative?
To have the competition build outside of our ecosystem and let them create value elsewhere?
- (+) Attracting new users, talent, and capital from outside thus helping grow the entire ecosystem centered around Bitcoin Cash.
Conceptually simple tools mean more potential builders.
- (+) Opportunity cost.
Success of tokens is a good measure of that.
Missed "revenue" in terms of user, talent, and capital inflows.
Missed ecosystem growth opportunity.
- (+) Future proof.
Since every group token is also a BCH output, it should seamlessly work with everything that can work with BCH, including Script and any future smart contract platform.
- (+) Every grouped output created also creates marginal demand for BCH.
- (+) Increased Bitcoin Cash adoption and utility, as the "first among equals" currency that powers the Bitcoin Cash blockchain.
- (+) Low risk to native currency security, as consensus rules are only added in
a self-contained way and toggled through a single opcode.
Existing rules are not changed.
- (-) Potential to speed up adoption.
While this benefits BCH, it could create problems of its own.
We don't see adoption creating problems as a realistic outcome, but this perspective is worth consideration, and it would be a good problem to have.

### 1 Node Stakeholders

- (-) Marginally more resource usage when compared to the simplest BCH UTXO.

#### 1.1 Miners

- (+) More fee revenue through increased demand for transactions.

#### 1.2 Big nodes

Exchanges, block explorers, backbone nodes, etc.

- (+) Easy to implement other currencies through same familiar design patterns that are now used for Bitcoin Cash alone.

#### 1.3 User nodes

Hobbyiest full nodes.

#### 1.4 Node developers

- (-) One-off workload increase
- (+) Later it just works out of the box, downstream developers create utility.

### 2 Userspace stakeholders

- (+) Barrier to entry to token usage lowered.

#### 2.1 Business users

- (+) Access to financial instruments previously not available, or access to them at a better price and quality than competition can provide.
- (+) Enables potential new business models.

#### 2.2 Userspace developers

- (+) Easy to implement other currencies through the same familiar design patterns that are now used for Bitcoin Cash alone.
- (+) With more growth there will be more opportunity for professional advancement within the ecosystem.

#### 2.3 Individual users

- (+) Get to enjoy the best peer-to-peer electronic cash system!

### 3 Holders and speculators

#### 3.1 Long

- (+) More adoption correlates with increase in price.
- (+) Even other tokens marginally increase demand for BCH, so the more
transactions of any kind, the better.

#### 3.2 Short

- (+) More liquidity in the market. Following Bitcoin Cash may help you decide to switch side and benefit from it!

### 4 Opinion holders who are not stakeholders

We are hoping developments like this will move you towards becoming stakeholders!

## Statements

[2021-04-02 General Protocols Statement on "CHIP-2021-02 Group Tokenization for Bitcoin Cash"](https://read.cash/@GeneralProtocols/general-protocols-statement-on-chip-2021-02-group-tokenization-for-bitcoin-cash-e7df47a0)

>A lot of work and research has gone into the technical specification of this CHIP. However, in its current form, GP does not think it is simple / minimal enough to be considered for acceptance into the network. Furthermore, important sections about costs, risks and alternatives are incomplete or have wrongfully been asserted as non-existent. GP would like to see a minimum viable change isolated from the current large scope, and then a deeper evaluation of costs, risks and alternatives.

[2021-04-05 Burak, SwapCash](https://youtu.be/f9wTZzNk1ro?t=4319)

>GROUP tokens are by far the most streamlined proposal to achieving token-supported covenants which could definitely help Bitcoin Cash unlocking its potential to mass adoption. Group tokens are not only needed for gaining user adoption, but also developer adoption. Builders need more scripting capabilities to building meaningful dapps of which Group proposal could definitely contribute to.

[2021-04-06 MaxHastings, Mint Slp](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/29?u=bitcoincashautist)

>As a developer and co-founder of Mint SLP https://mintslp.com/ 1

>I believe miner validated tokens is a smart step in the right direction! The Group Token proposal seems like a big improvement over SLP.

>Thank you @andrewstone and others involved for working on and pushing this feature into BCH.

>I hope the community can come together to support the Group Token proposal or something similar! You have my support for what it is worth!

## Future Work

[Full future functional specification and upgrade path.](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit)

## Discussions

[CHIP Discussion](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311)
[Previous Discussion](https://bitcoincashresearch.org/t/native-group-tokenization/278)

## Copyright

MIT Licensed

## Credits

Satoshi Nakamoto, invention of [Bitcoin: A Peer-to-Peer Electronic Cash System](https://www.bitcoin.com/bitcoin.pdf).  
Andrew Stone, authoring the [original Group Tokenization proposal](https://www.nextchain.cash/grouptokens) and full showcase implementation.

The CHIP however, is really authored by everyone who's interacted with it.
I'd like to personally thank:

- Andrew Stone, for inspiration, being supportive of the initial CHIP versions, and private consultations,
- Thomas Zander, for inspiration, being the "first responder" to the proposal, and helping me realize the importance of attention to details and their impact on the full product,
- matricz, for the [inspiring post](https://read.cash/@mtrycz/how-i-envision-decentralized-cooperation-on-bitcoin-cash-9876b9e9) on how decentralized cooperation should work,
- imaginary_username, for looking at it with a critical mind and helping me find a better way forward,
- BigBlockIfTrue, for early interaction with the proposal and explaining to me the nuances of locking and unlocking script,
- emergent_reasons, for setting the bar high for CHIPs and asking for steel-man arguments,
- Calin Culianu, for kicking the CHIP into higher gear with the PFX_GROUP approach,
- Emil Oldenburg, for having the patience to help me see the advantage of having a single baton and enforcing metadata locks.

and everyone else who interacted with this proposal and by doing so helped bring it to the level where it currently is.
If I forgot someone, please let me know!

Sorted alphabetically: AsashoryuMaryBerry, Burak, Estebansaa, FerriestaPatronum, Fixthetracking, Freedom-phoenix, Freetrader, George Donelly, Jason Dreyzehner, Jonathan Silverblood, Licho, Marc De Mesel, MobTwo, Mr. Korrupto, Saddit42, Shadow Of Harbringer, Supremelummox, Tl121, Tula_s, Twoethy.
